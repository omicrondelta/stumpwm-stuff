;;; Set lexical
(in-package :stumpwm)

;;; Load ASDF compliant StumpWM modules
(ql:quickload '(:ttf-fonts
                :quicklisp-slime-helper))

;;; Global window manager settings
(setf *mode-line-foreground-color* "gray"
      *mode-line-background-color* "black"
      *mode-line-border-color* "black"
      *message-window-gravity* :top-right
      *window-border-style* :none
      *normal-border-width* 0
      *transient-border-width* 1
      *float-window-tile-height* 0
      *mode-line-timeout* 1
      *mouse-focus-policy* :click)

;;; Window manager colors
(set-bg-color "black")
(set-fg-color "gray")
(set-border-color "gray")
(set-focus-color "#8C5760")
(set-unfocus-color "black")
(set-frame-outline-width 1)

;;; Global mode-line structure
(setq *screen-mode-line-format*
      (list
       "^2*StumpWM ^7*| "
       '(:eval (format nil "^3*~A" (slot-value (current-group) 'name)))
       " ^7*| ^6*%W"))

;;; Variables for custom functions
(defvar *useless-gaps-size* 5)
(defvar *useless-gaps-on* nil)
(defvar *btrfs-thread* nil)
(defvar *run-once* nil)

;;; Macros
(defmacro root-key (key command)
  `(define-key *root-map* (kbd, key), command))

(defmacro top-key (key command)
  `(define-key *top-map* (kbd ,key) ,command))

(defmacro root-keys (&rest keys)
  (let ((ks (mapcar #'(lambda (k)
			(cons 'root-key k)) keys)))
    `(progn ,@ks)))

(defmacro top-keys (&rest keys)
  (let ((ks (mapcar #'(lambda (k)
			(cons 'top-key k)) keys)))
    `(progn ,@ks)))

;;; Lisping the window manager with new functions
(defun shift-windows-forward (frames win)
  (when frames
    (let ((frame (car frames)))
      (shift-windows-forward (cdr frames) (frame-window frame))
      (when win
        (pull-window win frame)))))

(defun echo-urgent-window (target)
  (message-no-timeout "~a has a message for you." (window-title target)))

(defun maximize-window (win)
  (multiple-value-bind (x y wx wy width height border stick)
      (geometry-hints win)
    (if *useless-gaps-on*
        (setf width (- width (* 2 *useless-gaps-size*))
              height (- height (* 2 *useless-gaps-size*))
              x (+ x *useless-gaps-size*)
              y (+ y *useless-gaps-size*)))
    (dformat 4 "maximize window ~a x: ~d y: ~d width: ~d height: ~d border: ~d stick: ~s~%" win x y width height border stick)
    (set-window-geometry win :x wx :y wy :width width :height height :border-width 0)
    (xlib:with-state ((window-parent win))
                     (setf (xlib:drawable-x (window-parent win)) x
                           (xlib:drawable-y (window-parent win)) y
                           (xlib:drawable-border-width (window-parent win)) border)
                     (if (or stick (find *window-border-style* '(:tight :none)))
                         (setf (xlib:drawable-width (window-parent win)) (window-width win)
                               (xlib:drawable-height (window-parent win)) (window-height win))
                       (let ((frame (window-frame win)))
                         (setf (xlib:drawable-width (window-parent win)) (- (frame-width frame)
                                                                            (* 2 (xlib:drawable-border-width (window-parent win)))
                                                                            (if *useless-gaps-on*
                                                                                (* 2 *useless-gaps-size*)
                                                                              0))
                               (xlib:drawable-height (window-parent win)) (- (frame-display-height (window-group win) frame)
                                                                             (* 2 (xlib:drawable-border-width (window-parent win)))
                                                                             (if *useless-gaps-on*
                                                                                 (* 2 *useless-gaps-size*)
                                                                               0)))))
                     (xlib:change-property (window-xwin win) :_NET_FRAME_EXTENTS (list wx
                                                                                       wy
                                                                                       (- (xlib:drawable-width (window-parent win))
                                                                                          width
                                                                                          wx)
                                                                                       (- (xlib:drawable-height (window-parent win))
                                                                                          height
                                                                                          wy))
                                           :cardinal 32))))

(defun reset-all-windows ()
  (let ((windows (mapcan (lambda (g)
                           (mapcar (lambda (w) w) (sort-windows g)))
                         (sort-groups (current-screen)))))
    (mapcar (lambda (w)
              (if (string= (class-name (class-of w)) "TILE-WINDOW")
                  (maximize-window w))) windows)))

(defun btrfs-cleanup-thread ()
  (let* ((subvolumes
          (coerce (run-shell-command "echo -n $(grep subvol /etc/fstab | awk '{print $2}')" t) 'string))
         (subvolumes-list (loop for i = 0 then (1+ j)
                                as j = (position #\Space subvolumes :start i)
                                collect (subseq subvolumes i j)
                                while j)))
    (message "Btrfs filesystem maintenance initiated.")
    (dolist (directory 'subvolumes-list)
      (let ((defrag-file-data (format nil "btrfs filesystem defragment -rf ~A" directory))
            (defrag-directory-metadata (format nil "find ~A -xdev -type d -print -exec btrfs filesystem defragment -f '{}' \\; 2> /dev/null" directory)))
        (dolist (command '(defrag-file-data defrag-directory-metadata))
          (run-shell-command command))))
    (message "Btrfs filesystem maintenance complete.")))

;;; StumpWM command definitions
(defcommand btrfs-cleanup () ()
  (unless (sb-thread:thread-alive-p *btrfs-thread*)
    (setf *btrfs-thread* (sb-thread:make-thread (lambda () (btrfs-cleanup-thread))
                                                :name "btrfs-cleanup-thread"))
    (message "Btrfs filesystem maintenance in progress...")))

(defcommand rotate-windows () ()
  (let* ((frames (group-frames (current-group)))
         (win (frame-window (car (last frames)))))
    (shift-windows-forward frames win)))

(defcommand toggle-split () ()
  (let* ((group (current-group))
         (cur-frame (tile-group-current-frame group))
         (frames (group-frames group)))
    (if (eq (length frames) 2)
        (progn (if (or (neighbour :left cur-frame frames)
                      (neighbour :right cur-frame frames))
                   (progn
                     (only)
                     (vsplit))
                 (progn
                   (only)
                   (hsplit))))
      (message "^2*GURU MEDITATION: ^7*At least two frames are required in this group to use this function"))))

(defcommand gaps () ()
  (setf *useless-gaps-on* (null *useless-gaps-on*))
  (reset-all-windows))

(defcommand gselect-or-create (group-number) ((:number "Group number: "))
  (gselect (or (select-group (current-screen) (format nil "~A" group-number))
              (let ((group (add-group (current-screen)
                                      (format nil "Default<~A>" group-number)
                                      :background t)))
                (setf (group-number group) group-number)
                group))))

(defcommand mode-line-toggle () ()
  (toggle-mode-line (current-screen) (current-head)))

 ;;; Callback hooks
(add-hook *urgent-window-hook* 'echo-urgent-window)

;;; Key-bindings
(set-prefix-key (kbd "s-w"))

(top-keys ("s-g" "gaps")
	  ("s-f" "exec firefox")
	  ("s-S-RET" "exec xfce4-terminal")
	  ("s-r" "exec dmenu_run -p 'Execute: ' -nb '#09090D' -nf '#B8B8C8' -fn 'Droid Sans-11' -sb '#58698C' -sf '#B8B8C8'")
	  ("s-e" "exec gentoo")
	  ("s-Up" "move-focus up")
	  ("s-Down" "move-focus down")
	  ("s-Left" "move-focus left")
	  ("s-Right" "move-focus right"))

(dotimes (i 9)
  (define-key *top-map* (kbd (format nil "s-~A" (1+ i)))
    (format nil "gselect-or-create ~A" (1+ i))))

(root-keys ("Delete" "repack-window-numbers")
	   ("I" "show-window-properties")
	   ("|" "toggle-split")
	   ("~" "rotate-windows")
	   ("s-]" "mode-line-toggle"))

;;; Cache local TTF/OTF fonts and set main wm font
(xft:cache-fonts)
(set-font (make-instance 'xft:font
                         :family "Noto Sans"
                         :subfamily "Bold"
                         :size 10))

;;; Commands and functions to be run only on first startup of window manager
(unless *run-once*
  (progn
    (dolist (command '("xsetroot -cursor_name left_ptr"
                       "feh --bg-fill ~/.bg.jpg"
                       "xcompmgr -c -C -t-5 -l-5 -r4.2 -o.55"
                       "redshift"))
      (run-shell-command command))
    (swank-loader:init)
    (swank:create-server :port 4005
                         :style swank:*communication-style*
                         :dont-close t)
    
    (setf *run-once* t)))

;; Local variables:
;; coding: utf-8
;; mode: emacs-lisp
;; End:
